*Tema projekta je DRUSTVENA MREZA u kojoj korisnici mogu da postavljaju neke postove (za sada samo text). 

*Glavna karakteristika je pracenje (follow) hashtag-ova i svih postova koji se pojavljuju pod tim hashtag-om. 

*Korisnik moze da glasa (vote) za neki post, moze da komentarise svoje i tudje postove i da brise svoj post. Od basic informacija, korisnik moze da menja Bio (biografija, opis) klikom na slicicu olovkice pored na stranici #myprofile. 

*Stranice su:
- #hash koja je login stranica tj. news feed stranica kada se korisnik ulogje
- sign up stranica za kreiranje novog korisnika
- #hashtags stranica prikazuje listu svih hashtag-ova sortiranih po broju postova
- #myprofile stranica prikazuje informacije korisnika, postove korisnika i na njoj je moguce dodati novi post. 

*Uz dodavanje posta, upisom imena hashtag-a u text box ispod i klikom na enter, dodaje se upisani (1) hashtag u listu hashtag-ova tog posta, za naredni se ponavlja isti proces. 

*Klikom na dugme Post this, post se dodaje u listu postova tog korisnika i tih hashtagova (dodaju se ako ne postoje). Brisanje postova je moguce klikom na X u gornjem desnom uglu posta na #myprofile stranici. 

*Dodavanje komentara se vrsi upisom teksta u polje ispod posta i klikom na dugme Comment.

*Kolekcija Users pamti podatke o korisnicima (listu postova, listu hashtag-ova koje prati korisnik, username, password, ime). 

*Kolekcija Hashtags pamti informacije o hashtag-ovima (listu postova, ime hashtag-a).

Exportovane kolekcije i struktura baze je u folderu Hash/Baza. 

IMPORTANT: Neki od foldera se zovu Moviegram iz razloga sto nam je ime aplikacije u startu bilo Moviegram, ali smo na kraju odlucili da to ipak bude Hash, pa da ne bi doslo do problema kod pokretanja solution-a i aplikacije, nismo menjali imena. :)

Tamara Stanojevic 15387 i Ilija Vucic 15470