﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class PostGram
    {
        [BsonElement("Id")]
        public ObjectId Id { get; set; }
        [BsonElement("PostId")]
        public Guid PostId { get; set; }

        [BsonElement("OwnerId")]
        public ObjectId OwnerId { get; set; }
        [BsonElement("OwnerUsername")]
        public string OwnerUsername { get; set; }


        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }
        [BsonElement("Body")]
        public string Body { get; set; }
        [BsonElement("Comments")]
        public List<CommentsGram> Comments { get; set; }
        [BsonElement("Likes")]
        public List<Tuple<ObjectId, string>> Likes { get; set; }
        [BsonElement("Hashtags")]
        public List<string> Hashtags { get; set; }

        public PostGram()
        {
            PostId = Guid.NewGuid();
            Likes = new List<Tuple<ObjectId, string>>();
            Comments = new List<CommentsGram>();
            Hashtags = new List<string>();
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}