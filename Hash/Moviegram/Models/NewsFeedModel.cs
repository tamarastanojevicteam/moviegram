﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class NewsFeedModel
    {
        public UserGram user;
        public List<PostGram> followedPosts;

        public NewsFeedModel()
        {
            user = new UserGram();
            followedPosts = new List<PostGram>();
        }
    }
}