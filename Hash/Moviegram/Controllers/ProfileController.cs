﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Moviegram.App_Start;
using Moviegram.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Moviegram.Controllers
{
    public class ProfileController : Controller
    {
        MongoContext _mongoContext;
        UserGram _user;
        
        public ProfileController()
        {
            _mongoContext = new MongoContext();
            _user = new UserGram();
        }

        // GET: Profile
        public ActionResult Index()
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            UserGram user = new UserGram();
            if (id != String.Empty)
            {
                var query1 = Query.EQ("_id", new ObjectId(id));
                var count1 = _document.FindAs<UserGram>(query1).Count();
                if (count1 == 1)
                {
                    user = (UserGram)_document.FindAs<UserGram>(query1).FirstOrDefault();
                }
            }

            NewsFeedModel model = new NewsFeedModel();
            model.user = user;

            var _documentFromHashtag = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
            foreach (var tag in user.FollowedTags)
            {
                var query2 = Query.EQ("_id", tag.Item1);
                var count2 = _documentFromHashtag.FindAs<HashtagGram>(query2).Count();

                if(count2==1)
                {
                    HashtagGram hashtag = (HashtagGram)_documentFromHashtag.FindAs<HashtagGram>(query2).FirstOrDefault();
                    if (model.followedPosts.Count == 0)
                        model.followedPosts = hashtag.Posts;
                    else
                    {
                        foreach(PostGram post in hashtag.Posts)
                        {
                            if(!model.followedPosts.Exists(x=>x.PostId==post.PostId))
                            {
                                model.followedPosts.Add(post);
                            }
                        }
                    }
                }
            }

            model.followedPosts.OrderByDescending(x => x.Timestamp);
            //var userDetails = _mongoContext._database.GetCollection<UserGram>(ConfigurationManager.AppSettings["MongoDatabaseName"]).FindAll().ToList();
            
            return View(model);
        }

        public ActionResult LogOut()
        {
            ConfigurationManager.AppSettings["Id"] = "";
            ConfigurationManager.AppSettings["Username"] = "";
            ConfigurationManager.AppSettings["Password"] = "";

            return RedirectToAction("Index");
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string username, string password)
        {
            try
            {
                var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
                var query = Query.And(Query.EQ("Username", username), Query.EQ("Password", password));
                var count = _document.FindAs<UserGram>(query).Count();

                if (count == 1)
                {
                    _user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                    ConfigurationManager.AppSettings["Username"] = _user.Username;
                    ConfigurationManager.AppSettings["Password"] = _user.Password;
                    ConfigurationManager.AppSettings["Id"] = _user.Id.ToString();
                }
                else
                {
                    TempData["Message"] = "Wrong username or password.";
                    return View("LogIn");
                }

                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return View();
            }
        }

        public ActionResult UserProfile()
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(id));
            var count = _document.FindAs<UserGram>(query).Count();

            if (count == 1)
            {
                UserGram user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                return View(user);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Hashtags()
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
            var query = Query.Empty;
            var count = _document.FindAs<HashtagGram>(query).Count();

            var _model = _document.FindAs<HashtagGram>(query).ToList<HashtagGram>();
            
            return View(_model);
        }

        public ActionResult FollowHashtag(string hashtagId, string hashtagName)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(id));
            var count = _document.FindAs<UserGram>(query).Count();

            if(count==1)
            {
                UserGram user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                user.FollowedTags.Add(new Tuple<ObjectId, string>(new ObjectId(hashtagId), hashtagName));
                _document.Update(query, Update.Replace(user), UpdateFlags.None);
            }

            return RedirectToAction("Index");
        }

        public ActionResult UnfollowHashtag(string hashtagId, string hashtagName)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(id));
            var count = _document.FindAs<UserGram>(query).Count();

            if (count == 1)
            {
                UserGram user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                user.FollowedTags.Remove(new Tuple<ObjectId, string>(new ObjectId(hashtagId), hashtagName));
                _document.Update(query, Update.Replace(user), UpdateFlags.None);
            }

            return RedirectToAction("Index");

        }

        public ActionResult OpenHashtag(string hashtagId)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
            var query = Query.EQ("_id", new ObjectId(hashtagId));
            var count = _document.FindAs<HashtagGram>(query).Count();

            HashtagViewModel model = new HashtagViewModel();

            if (count == 1)
            {
                HashtagGram hashtag = (HashtagGram)_document.FindAs<HashtagGram>(query).FirstOrDefault();
                model.hashtag = hashtag;
            }

            var _documentFromUsers = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query1 = Query.EQ("_id", new ObjectId(id));
            var count1 = _documentFromUsers.FindAs<UserGram>(query1).Count();

            if(count1==1)
            {
                UserGram user = (UserGram)_documentFromUsers.FindAs<UserGram>(query1).FirstOrDefault();
                model.user = user;
                model.following = user.FollowedTags.Exists(x => x.Item2.Equals(model.hashtag.Name));
            }

            return View(model);
        }

        public ActionResult LikePost(string postId)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _documentHashtag = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
            var _documentUsers = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);

            var queryFromHashtag = Query.Empty;
            var count1 = _documentHashtag.FindAs<HashtagGram>(queryFromHashtag).Count();
            if (count1 > 0)
            {
                List<HashtagGram> list = (List<HashtagGram>)_documentHashtag.FindAs<HashtagGram>(queryFromHashtag).ToList();
                foreach (HashtagGram tag in list)
                {
                    PostGram post = tag.Posts.Find(x => x.PostId == Guid.Parse(postId));
                    if (post != null)
                    {
                        post.Likes.Add(new Tuple<ObjectId, string>(new ObjectId(ConfigurationManager.AppSettings["Id"]), ConfigurationManager.AppSettings["Username"]));

                        var query = Query.EQ("_id", tag.Id);
                        _documentHashtag.Update(query, Update.Replace<HashtagGram>(tag), UpdateFlags.None);
                    }
                }            
            }

            var queryFromUsers = Query.Empty;
            var count2 = _documentUsers.FindAs<UserGram>(queryFromUsers).Count();
            if (count2 > 0)
            {
                List<UserGram> list = (List<UserGram>)_documentUsers.FindAs<UserGram>(queryFromUsers).ToList();
                foreach (UserGram user in list)
                {
                    PostGram post = user.Posts.Find(x => x.PostId == Guid.Parse(postId));
                    if (post != null)
                    {
                        post.Likes.Add(new Tuple<ObjectId, string>(new ObjectId(ConfigurationManager.AppSettings["Id"]), ConfigurationManager.AppSettings["Username"]));

                        var query = Query.EQ("_id", user.Id);
                        _documentUsers.Update(query, Update.Replace<UserGram>(user), UpdateFlags.None);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult OpenHashtagByName(string hashtagName)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return RedirectToAction("LogIn");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
            var query = Query.EQ("Name", hashtagName);
            var count = _document.FindAs<HashtagGram>(query).Count();

            HashtagViewModel model = new HashtagViewModel();

            if (count == 1)
            {
                HashtagGram hashtag = (HashtagGram)_document.FindAs<HashtagGram>(query).FirstOrDefault();
                model.hashtag = hashtag;
            }

            var _documentFromUsers = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query1 = Query.EQ("_id", new ObjectId(id));
            var count1 = _documentFromUsers.FindAs<UserGram>(query1).Count();

            if (count1 == 1)
            {
                UserGram user = (UserGram)_documentFromUsers.FindAs<UserGram>(query1).FirstOrDefault();
                model.user = user;
                model.following = user.FollowedTags.Exists(x => x.Item2.Equals(model.hashtag.Name));
            }

            return View("OpenHashtag",model);
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult Create(string username, string password, string fullname, string bio)
        {
            try
            {
                UserGram user = new UserGram();
                user.Username = username;
                user.Password = password;
                user.FullName = fullname;
                user.Bio = bio;
                
                var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
                var query = Query.EQ("Username", user.Username);
                var count = _document.FindAs<UserGram>(query).Count();
                if(count==0)
                {
                    var result = _document.Insert(user);
                }
                else
                {
                    return View("Create", user);
                }

                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return View();
            }
        }

        //public ActionResult CreatePost(string ownerId, string username)
        //{
        //    PostGram model = new PostGram();
        //    model.OwnerId = new ObjectId(ownerId);
        //    model.OwnerUsername = username;

        //    return View(model);
        //}
       
        [HttpPost]
        public ActionResult CreatePost(string postText, List<string> hashtags)
        {
            try
            {
                string username = ConfigurationManager.AppSettings["Username"];
                string password = ConfigurationManager.AppSettings["Password"];
                string id = ConfigurationManager.AppSettings["Id"];

                if ((username == String.Empty))
                {
                    return RedirectToAction("LogIn");
                }

                PostGram post = new PostGram();
                post.Body = postText;
                post.Hashtags = hashtags;

                post.OwnerId = new ObjectId(id);
                post.OwnerUsername = username;

                var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
                var query = Query.EQ("_id", post.OwnerId);
                var count = _document.FindAs<UserGram>(query).Count();

                
                //add post to user
                UserGram user = new UserGram();
                if (count == 1)
                {
                    user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                    user.Posts.Add(post);
                    _document.Update(query, Update.Replace(user), UpdateFlags.None);
                }

                //add post to hashtag
                foreach(string tag in hashtags)
                {
                    _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
                    query = Query.EQ("Name", tag);
                    count = _document.FindAs<HashtagGram>(query).Count();

                    if(count>0)
                    {
                        HashtagGram hashtag = new HashtagGram();
                        hashtag = (HashtagGram)_document.FindAs<HashtagGram>(query).FirstOrDefault();
                        hashtag.Posts.Add(post);
                        _document.Update(query, Update.Replace(hashtag), UpdateFlags.None);
                    }
                    else
                    {
                        HashtagGram hashtag = new HashtagGram();
                        hashtag.Name = tag;
                        hashtag.Posts.Add(post);
                        _document.Insert<HashtagGram>(hashtag);
                    }
                }

                return RedirectToAction("UserProfile", "Profile");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult CreateComment(string comment, string ownerId, string postId)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return Json("error");
            }

            CommentsGram comm = new CommentsGram();
            comm.OwnerId = new ObjectId(id);
            comm.OwnerUsername = username;
            comm.Text = comment;

            //get post from userscollection to add comment from current user
            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(ownerId));
            var count = _document.FindAs<UserGram>(query).Count();

            if(count==1)
            {
                UserGram owner = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                PostGram post = owner.Posts.Find(x => x.PostId == Guid.Parse(postId));
                post.Comments.Add(comm);
                _document.Update(query, Update.Replace(owner), UpdateFlags.None);

                foreach (string tag in post.Hashtags)
                {
                    var _documentFromHashtag = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);
                    var query1 = Query.EQ("Name", tag);
                    var count1 = _documentFromHashtag.FindAs<HashtagGram>(query1).Count();

                    if (count1 == 1)
                    {
                        HashtagGram hashtag = (HashtagGram)_documentFromHashtag.FindAs<HashtagGram>(query1).FirstOrDefault();
                        hashtag.Posts.Find(x => x.PostId == Guid.Parse(postId)).Comments.Add(comm);

                        _documentFromHashtag.Update(query1, Update.Replace(hashtag), UpdateFlags.None);
                    }
                }
            }

            return Json("ok");
        }

        [HttpPost]
        public JsonResult UpdateProfileBio(string bio)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return Json("error");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(id));
            var count = _document.FindAs<UserGram>(query).Count();

            if(count==1)
            {
                UserGram user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                user.Bio = bio;
                _document.Update(query, Update.Replace(user), UpdateFlags.None);
            }

            return Json(bio);
        }

        [HttpPost]
        public JsonResult DeletePost(string postId)
        {
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string id = ConfigurationManager.AppSettings["Id"];

            if ((username == String.Empty))
            {
                return Json("error");
            }

            var _document = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["UsersCollection"]);
            var query = Query.EQ("_id", new ObjectId(id));
            var count = _document.FindAs<UserGram>(query).Count();

            List<string> hashtags = new List<string>();

            if (count == 1)
            {
                UserGram user = (UserGram)_document.FindAs<UserGram>(query).FirstOrDefault();
                foreach (PostGram post in user.Posts)
                {
                    if(post.PostId==Guid.Parse(postId))
                    {
                        hashtags = post.Hashtags;
                        user.Posts.Remove(post);
                        break;
                    }
                }
                _document.Update(query, Update.Replace(user), UpdateFlags.None);
            }

            var _documentFromHashtag = _mongoContext._database.GetCollection<BsonDocument>(ConfigurationManager.AppSettings["HashtagsCollection"]);

            foreach(string tag in hashtags)
            {
                var query1 = Query.EQ("Name", tag);
                var count1 = _documentFromHashtag.FindAs<HashtagGram>(query1).Count();

                if(count1==1)
                {
                    HashtagGram hashtag = (HashtagGram)_documentFromHashtag.FindAs<HashtagGram>(query1).FirstOrDefault();
                    hashtag.Posts.RemoveAll(x => x.PostId == Guid.Parse(postId));
                    _documentFromHashtag.Update(query1, Update.Replace(hashtag), UpdateFlags.None);
                }
            }
            
            return Json("ok");
        }

        // GET: Profile/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Profile/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
